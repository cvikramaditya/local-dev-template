const gulp = require("gulp"),
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  autoprefixer = require("autoprefixer"),
  cssnano = require("cssnano"),
  browserSync = require("browser-sync").create(),
  { exec } = require("child_process");

function scripts(cb) {
  exec(
    "parcel build ./app/js/**/*.js --public-url /js --out-dir ./dist/js",
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
      }
      console.log(`${stdout}`);
      cb();
    }
  );
}

function styles() {
  return gulp
    .src("app/sass/**/*.scss")
    .pipe(
      sass({
        includePaths: ["node_modules"],
      })
    )
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(gulp.dest("dist/css"));
}

function html() {
  return gulp.src("app/html/**/*.html").pipe(gulp.dest("dist"));
}

function watch(cb) {
  gulp.watch("app/html/**/*.*", gulp.series(html, browserSync.reload));
  gulp.watch("app/sass/**/*.*", gulp.series(styles, browserSync.reload));
  gulp.watch("app/js/**/*.*", gulp.series(scripts, browserSync.reload));
  // cb();
}

function server(cb) {
  browserSync.init({
    server: "./dist",
  });
}

exports.default = gulp.series(
  gulp.parallel(styles, scripts, html),
  gulp.parallel(server, watch)
);
